using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using System;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;


namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoDbEmployeeRepository
        : IRepository<Employee>
    {
        private readonly IMongoCollection<Employee> _entityCollection;
        private string location = "en_US";

        public MongoDbEmployeeRepository(IMongoCollection<Employee> entityCollection)
        {
            _entityCollection = entityCollection;
        }


        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(_ => true,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.LastName)
                .ToListAsync(cancellation.Token);
        }

        public async Task<Employee> GetByIdAsync(Guid id)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(l => l.Id == id,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    }).FirstOrDefaultAsync(cancellation.Token);
        }

        public async Task<IEnumerable<Employee>> GetRangeByIdsAsync(List<Guid> ids)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(l => ids.Contains(l.Id),
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.LastName)
                .ToListAsync(cancellation.Token);
        }

        public async Task<Employee> GetFirstWhere(Expression<Func<Employee, bool>> predicate)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            var result = await _entityCollection.Find(predicate,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .ToListAsync(cancellation.Token);

            return result == null ? null : result[0];
        }

        public async Task<IEnumerable<Employee>> GetWhere(Expression<Func<Employee, bool>> predicate)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(predicate,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.LastName)
                .ToListAsync(cancellation.Token);
        }

        public async Task AddAsync(Employee entity)
        {
            await _entityCollection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(Employee entity)
        {
            var filter = Builders<Employee>.Filter.Eq("Id", entity.Id);

            var updatestatement = Builders<Employee>.Update.Set("FirstName", entity.FirstName);
            updatestatement = updatestatement.Set("LastName", entity.LastName);
            updatestatement = updatestatement.Set("Email", entity.Email);
            updatestatement = updatestatement.Set("RoleId", entity.RoleId);

            var result = await _entityCollection.UpdateOneAsync(filter, updatestatement);
            if (result.IsAcknowledged == false)
                throw new Exception("Unable to update Employee " + entity.FullName);
        }

        public async Task DeleteAsync(Employee entity)
        {
            var result = await _entityCollection.DeleteOneAsync<Employee>( l => l.Id == entity.Id);
            if (result.IsAcknowledged == false)
                throw new Exception("Unable to delete Employee " + entity.FullName);
        }

    }
}