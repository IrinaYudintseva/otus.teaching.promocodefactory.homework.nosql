using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using System;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;


namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoDbRoleRepository
        : IRepository<Role>
    {
        private readonly IMongoCollection<Role> _entityCollection;
        private string location = "en_US";

        public MongoDbRoleRepository(IMongoCollection<Role> entityCollection)
        {
            _entityCollection = entityCollection; 
        }


        public async Task<IEnumerable<Role>> GetAllAsync()
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(_ => true,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.Name)
                .ToListAsync(cancellation.Token);
        }

        public async Task<Role> GetByIdAsync(Guid id)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(l => l.Id == id,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    }).FirstOrDefaultAsync(cancellation.Token);
        }

        public async Task<IEnumerable<Role>> GetRangeByIdsAsync(List<Guid> ids)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(l => ids.Contains(l.Id),
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.Name)
                .ToListAsync(cancellation.Token);
        }

        public async Task<Role> GetFirstWhere(Expression<Func<Role, bool>> predicate)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            var result = await _entityCollection.Find(predicate,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .ToListAsync(cancellation.Token);

            return result == null ? null : result[0];
        }

        public async Task<IEnumerable<Role>> GetWhere(Expression<Func<Role, bool>> predicate)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _entityCollection.Find(predicate,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.Name)
                .ToListAsync(cancellation.Token);
        }

        public async Task AddAsync(Role entity)
        {
            await _entityCollection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(Role entity)
        {
            var filter = Builders<Role>.Filter.Eq("Id", entity.Id);

            var updatestatement = Builders<Role>.Update.Set("Name", entity.Name);
            updatestatement = updatestatement.Set("Description", entity.Description);

            var result = await _entityCollection.UpdateOneAsync(filter, updatestatement);
            if (result.IsAcknowledged == false)
                throw new Exception("Unable to update Role " + entity.Name);
        }

        public async Task DeleteAsync(Role entity)
        {
            var result = await _entityCollection.DeleteOneAsync<Role>( l => l.Id == entity.Id);
            if (result.IsAcknowledged == false)
                throw new Exception("Unable to delete Role " + entity.Name);
        }

    }
}