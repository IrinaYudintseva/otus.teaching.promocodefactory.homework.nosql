﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests.Data
{
    public class EfTestDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _db;

        public EfTestDbInitializer(IMongoDatabase dataContext)
        {
            _db = dataContext;
        }

        public void InitializeDb()
        {
            var _employeeCollection = _db.GetCollection<Employee>("Employee");
            var _roleCollection = _db.GetCollection<Role>("Role");

            if (_roleCollection.EstimatedDocumentCount() > 0)
                return;

            _roleCollection.InsertManyAsync(TestDataFactory.Roles);
            _employeeCollection.InsertManyAsync(TestDataFactory.Employees);
        }

        public void CleanDb()
        {
            var _employeeCollection = _db.GetCollection<Employee>("Employee");
            var _roleCollection = _db.GetCollection<Role>("Role");

            if (_roleCollection.EstimatedDocumentCount() > 0)
                _roleCollection.DeleteMany(_ => true);

            if (_employeeCollection.EstimatedDocumentCount() > 0)
                _employeeCollection.DeleteMany(_ => true);
        }
    }
}