using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using System;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;


namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoDbPromoCodeRepository
        : IRepository<PromoCode>
    {
        private readonly IMongoCollection<PromoCode> _promocodeCollection;
        private string location = "en_US";

        public MongoDbPromoCodeRepository(IMongoCollection<PromoCode> promocodeCollection)
        {
            _promocodeCollection = promocodeCollection;
        }

        public async Task<IEnumerable<PromoCode>> GetAllAsync()
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _promocodeCollection.Find(_ => true,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.Code)
                .ToListAsync(cancellation.Token);
        }

        public async Task<PromoCode> GetByIdAsync(Guid id)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
           var result =  await _promocodeCollection.Find(l => l.Id == id,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    }).FirstOrDefaultAsync(cancellation.Token);

            return result;
        }

        public async Task<IEnumerable<PromoCode>> GetRangeByIdsAsync(List<Guid> ids)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _promocodeCollection.Find(l => ids.Contains(l.Id),
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.Code)
                .ToListAsync(cancellation.Token);
        }

        public async Task<PromoCode> GetFirstWhere(Expression<Func<PromoCode, bool>> predicate)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            var result = await _promocodeCollection.Find(predicate,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .ToListAsync(cancellation.Token);

            return result == null ? null : result[0];
        }

        public async Task<IEnumerable<PromoCode>> GetWhere(Expression<Func<PromoCode, bool>> predicate)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _promocodeCollection.Find(predicate,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.Code)
                .ToListAsync(cancellation.Token);
        }

        public async Task AddAsync(PromoCode promocode)
        {
            await _promocodeCollection.InsertOneAsync(promocode);
        }

        public async Task UpdateAsync(PromoCode promocode)
        {
            var filter = Builders<PromoCode>.Filter.Eq("Id", promocode.Id);

            var updatestatement = Builders<PromoCode>.Update.Set("Code", promocode.Code);
            updatestatement = updatestatement.Set("ServiceInfo", promocode.ServiceInfo);
            updatestatement = updatestatement.Set("BeginDate", promocode.BeginDate);
            updatestatement = updatestatement.Set("EndDate", promocode.EndDate);
            updatestatement = updatestatement.Set("PartnerId", promocode.PartnerId);
            updatestatement = updatestatement.Set("PreferenceId", promocode.PreferenceId);
            updatestatement = updatestatement.Set("CustomerIds", promocode.CustomerIds);

            var result = await _promocodeCollection.UpdateOneAsync(filter, updatestatement);
            if (result.IsAcknowledged == false)
                throw new Exception("Unable to update PromoCode " + promocode.Code);
        }

        public async Task DeleteAsync(PromoCode promocode)
        {
            var result = await _promocodeCollection.DeleteOneAsync<PromoCode>( l => l.Id == promocode.Id);
            if (result.IsAcknowledged == false)
                throw new Exception("Unable to delete PromoCode " + promocode.Code);
        }

    }
}