﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data
{
    public class EfTestDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _dataContext;

        public EfTestDbInitializer(IMongoDatabase dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            var customerCollection = _dataContext.GetCollection<Customer>("Customer");
            var preferenceCollection = _dataContext.GetCollection<Preference>("Preference");

            // если коллекции содержат данные, то выходим
            if (customerCollection.EstimatedDocumentCount() > 0 ||
                preferenceCollection.EstimatedDocumentCount() > 0)
                return;

            preferenceCollection.InsertManyAsync(TestDataFactory.Preferences);
            customerCollection.InsertManyAsync(TestDataFactory.Customers);
        }

        public void CleanDb()
        {
            var customerCollection = _dataContext.GetCollection<Customer>("Customer");
            var preferenceCollection = _dataContext.GetCollection<Preference>("Preference");
            var promocodeCollection = _dataContext.GetCollection<PromoCode>("PromoCode");

            if (preferenceCollection.EstimatedDocumentCount() > 0)
                preferenceCollection.DeleteMany(_ => true);

            if (customerCollection.EstimatedDocumentCount() > 0)
                customerCollection.DeleteMany(_ => true);

            if (promocodeCollection.EstimatedDocumentCount() > 0)
                promocodeCollection.DeleteMany(_ => true);
        }
    }
}